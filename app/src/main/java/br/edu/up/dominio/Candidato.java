package br.edu.up.dominio;

import android.util.Base64;

import org.ksoap2.serialization.KvmSerializable;
import org.ksoap2.serialization.MarshalBase64;
import org.ksoap2.serialization.PropertyInfo;

import java.util.Hashtable;

public class Candidato implements KvmSerializable {

	private String codigo;
	private String nome;
	private byte[] foto;
	private int votos;

	public Candidato() {
	}

	public Candidato(String codigo, String nome, byte[] foto) {
		this.setCodigo(codigo);
		this.setNome(nome);
		this.foto = foto;
	}

	public void addVoto() {
		this.votos++;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public int getVotos() {
		return votos;
	}

	public void setVotos(int votos) {
		this.votos = votos;
	}

	@Override
	public Object getProperty(int arg) {
		switch(arg)
		{
			case 0:
				return codigo;
			case 1:
				return nome;
			case 2:
				return Base64.encode(foto, Base64.DEFAULT);
			case 3:
				return votos;
		}
		return null;
	}

	@Override
	public int getPropertyCount() {
		return 4;
	}

	@Override
	public void setProperty(int index, Object value) {
		switch(index)
		{
			case 0:
				codigo = value.toString();
				break;
			case 1:
				nome = value.toString();
				break;
			case 2:
				foto = Base64.decode(value.toString(), Base64.DEFAULT);
				break;
			case 3:
				votos = Integer.parseInt(value.toString());
			    break;
		}
	}

	@Override
	public void getPropertyInfo(int index, Hashtable hashtable, PropertyInfo info) {
		switch(index)
		{
			case 0:
				info.type = PropertyInfo.STRING_CLASS;
				info.name = "codigo";
				break;
			case 1:
				info.type = PropertyInfo.STRING_CLASS;
				info.name = "nome";
				break;
			case 2:
				info.type = MarshalBase64.BYTE_ARRAY_CLASS;
				info.name = "foto";
				break;
			case 3:
				info.type = PropertyInfo.INTEGER_CLASS;
				info.name = "votos";
				break;
		}
	}
}