package br.edu.up.ws;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.edu.up.dominio.Candidato;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 18/10/2015
 */
public class ListaDeCandidatosAdapter extends ArrayAdapter<Candidato> {

    //Necessário para abrir o XML.
    private LayoutInflater inflater;

    public ListaDeCandidatosAdapter(Context ctx, List<Candidato> candidatos){
        super(ctx, R.layout.candidato_item, candidatos);
        this.inflater= LayoutInflater.from(ctx);
    }

    @Override
    public Candidato getItem(int position) {
        return super.getItem(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = inflater.inflate(R.layout.candidato_item, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imgFoto);
        TextView txtNome = (TextView) view.findViewById(R.id.txtNomeCandidato);
        TextView txtVotos = (TextView) view.findViewById(R.id.txtVotos);

        Candidato candidato = getItem(position);
        Bitmap bitmap = BitmapFactory.decodeByteArray(candidato.getFoto(), 0, candidato.getFoto().length);
        imageView.setImageBitmap(bitmap);
        txtNome.setText(candidato.getNome());
        txtVotos.setText(String.valueOf(candidato.getVotos()));

        return view;
    }
}
