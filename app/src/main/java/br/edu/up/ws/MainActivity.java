package br.edu.up.ws;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.widget.ListView;

import org.ksoap2.HeaderProperty;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import br.edu.up.dominio.Candidato;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();
        executarContagem();
        //new AsyncCallWS().execute();
    }

    void executarContagem() {
        mStatusChecker.run();
    }

    private Handler mHandler;
    private int mInterval = 6000;

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            new AsyncCallWS().execute();
            mHandler.postDelayed(mStatusChecker, mInterval);
        }
    };


    private class AsyncCallWS extends AsyncTask<Void, Void, List<Candidato>> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected List<Candidato> doInBackground(Void... params) {
            return getClassificacaoWS();
        }

        @Override
        protected void onPostExecute(List<Candidato> candidatos) {

            ListaDeCandidatosAdapter adapter = new ListaDeCandidatosAdapter(MainActivity.this, candidatos);
            ListView listView = (ListView) findViewById(R.id.listViewCandidatos);
            listView.setAdapter(adapter);

        }
    }


    String TAG = "Response";
    String getCel;
    SoapPrimitive resultString;


    String METHOD = "getCandidatosClassificados";
    String ACTION = "http://ws.up.edu.br/UrnaWS/getCandidatosClassificadosRequest";
    String URWSDL = "http://up.cloudapp.net/urna/ws-soap?wsdl";
    String NSPACE = "http://ws.up.edu.br/";

    public List<Candidato> getClassificacaoWS() {

        List<Candidato> lista = new ArrayList<>();
        try {
            Vector<SoapObject> response = InvokeMethod(URWSDL, METHOD);
            for (SoapObject object : response) {
                Candidato candidato = new Candidato();
                candidato.setCodigo(object.getProperty(0).toString());
                candidato.setFoto(Base64.decode(object.getProperty(1).toString(), Base64.DEFAULT));
                candidato.setNome(object.getProperty(2).toString());
                candidato.setVotos(Integer.parseInt(object.getProperty(3).toString()));
                lista.add(candidato);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "Error: " + ex.getMessage());
        }
        return lista;
    }

    public static Candidato[] RetrieveFromSoap(SoapObject soap)
    {
        Candidato[] candidatos = new Candidato[soap.getPropertyCount()];
        for (int i = 0; i < candidatos.length; i++) {
            SoapObject pii = (SoapObject)soap.getProperty(i);
            Candidato candidato = new Candidato();
            candidato.setCodigo(pii.getProperty(0).toString());
            candidato.setFoto(Base64.decode(pii.getProperty(1).toString(), Base64.DEFAULT));
            candidato.setNome(pii.getProperty(2).toString());
            candidato.setVotos(Integer.parseInt(pii.getProperty(3).toString()));
            candidatos[i] = candidato;
        }
        return candidatos;
    }

    public Vector<SoapObject> InvokeMethod(String URL,String MethodName)
    {
        //SoapObject request = new SoapObject(NSPACE, METHOD);
        SoapObject request = GetSoapObject(MethodName);
        SoapSerializationEnvelope envelope = GetEnvelope(request);
        return  MakeCall(URL,envelope,NSPACE,MethodName);
    }

    public SoapObject GetSoapObject(String MethodName)
    {
        return new SoapObject(NSPACE,MethodName);
    }

    public static SoapSerializationEnvelope GetEnvelope(SoapObject Soap)
    {
        //SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        //soapEnvelope.setOutputSoapObject(request);
        //soapEnvelope.addMapping(NSPACE, "Candidato", new Candidato().getClass());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        //envelope.dotNet = true;
        envelope.setOutputSoapObject(Soap);
        return envelope;
    }

    public Vector<SoapObject> MakeCall(String URL, SoapSerializationEnvelope Envelope, String NAMESPACE, String METHOD_NAME)
    {
        HttpTransportSE transport = new HttpTransportSE(URL);

        try
        {
            List<HeaderProperty> header = new ArrayList<>();
            header.add(new HeaderProperty("Content-Type", "application/soap+xml; charset=utf-8"));
            transport.call(ACTION, Envelope, header);
            Vector<SoapObject> response = (Vector<SoapObject>) Envelope.getResponse();
            //SoapObject response = (SoapObject)Envelope.getResponse();
            return response;
        }
        catch(Exception e)
        {
            e.printStackTrace();

        }
        return null;
    }


    public void contarBkp() {

        String METHOD = "getCandidato";
        String ACTION = "http://ws.up.edu.br/UrnaWS/getCandidatoRequest";
        String URWSDL = "http://192.168.1.5:8080/urna/ws-soap?wsdl";
        String NSPACE = "http://ws.up.edu.br/";

        try {

            SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
            SoapObject request = new SoapObject(NSPACE, METHOD);
            request.addProperty("codigo", "11");
            soapEnvelope.setOutputSoapObject(request);
            soapEnvelope.addMapping(NSPACE, "Candidato", new Candidato().getClass());

            HttpTransportSE transport = new HttpTransportSE(URWSDL);
            List<HeaderProperty> headerList = new ArrayList<HeaderProperty>();
            headerList.add(new HeaderProperty("Content-Type", "application/soap+xml; charset=utf-8"));

            transport.call(ACTION, soapEnvelope, headerList);
            SoapObject response = (SoapObject)soapEnvelope.getResponse();

            Candidato candidato = new Candidato();
            candidato.setCodigo(response.getProperty(0).toString());
            candidato.setFoto(Base64.decode(response.getProperty(1).toString(), Base64.DEFAULT));
            candidato.setNome(response.getProperty(2).toString());
            candidato.setVotos(Integer.parseInt(response.getProperty(3).toString()));

            if (candidato != null){
                Log.d("SOAP", candidato.getNome());
            };

        } catch (Exception ex) {
            ex.printStackTrace();
            Log.e(TAG, "Error: " + ex.getMessage());
        }
    }
}
